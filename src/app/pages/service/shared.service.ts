import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private loadingStream = new BehaviorSubject<boolean>(false);
  isLoading = this.loadingStream.asObservable();

  constructor() { }

  startLoader(): void {
    this.loadingStream.next(true);
  }

  stopLoader(): void {
    this.loadingStream.next(false);
  }
}
