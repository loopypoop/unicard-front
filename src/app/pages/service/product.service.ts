import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(`http://localhost:4200/products`);
  }

  getById(id: number): Observable<any> {
    return this.http.get(`http://localhost:4200/products/${id}`);
  }
}
