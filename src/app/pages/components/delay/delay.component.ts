import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './delay.component.html',
  styleUrls: ['./delay.component.css']
})
export class DelayComponent implements OnInit {

  next = false;

  constructor() { }

  ngOnInit(): void {
  }

  toNext(): void {
    this.next = !this.next;
  }

}
