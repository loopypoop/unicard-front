import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.css']
})
export class DeliveryAddressComponent implements OnInit {

  isCityFocused = false;
  isStreetFocused = false;
  isEntranceFocused = false;
  isFloorFocused = false;
  isFlatFocused = false;

  city: string;
  street: string;
  entrance: string;
  floor: string;
  flat: string;
  issued: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  changeRePasswordVisibility(): void {
    // this.isRePasswordVisible = !this.isRePasswordVisible;
  }

  toNextForm(): void {
    this.router.navigate(['/sign-in/', 1]).then(() => {
      window.location.reload();
    });
  }
}
