import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.css']
})
export class DecisionComponent implements OnInit {

  payed = 1;
  next = false;

  constructor() { }

  ngOnInit(): void {
  }

  toNext(): void {
    this.next = !this.next;
  }

}
