import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contract',
  templateUrl: './confidentiality.component.html',
  styleUrls: ['./confidentiality.component.css']
})
export class ConfidentialityComponent implements OnInit {
  activeSection: string;
  constructor() { }

  ngOnInit(): void {
    this.activeSection = 'createAccSection';
  }

  scroll(divName: string): void {
    this.activeSection = divName;
    document.getElementById(divName).scrollIntoView();
  }
}
