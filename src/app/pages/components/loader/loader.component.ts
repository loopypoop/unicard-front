import {Component, Inject, OnInit} from '@angular/core';
import {delay} from 'rxjs/operators';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor(@Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
  }
  stopLoader(): void {
    this.sharedService.stopLoader();
  }

}
