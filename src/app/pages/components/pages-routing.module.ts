import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainProductComponent} from './main-product/main-product.component';
import {RegistrationComponent} from './registration/registration.component';
import {SmsConfirmationComponent} from './sms-confirmation/sms-confirmation.component';
import {DecisionComponent} from './decision/decision.component';
import {LoaderComponent} from './loader/loader.component';
import {ContractComponent} from './contract/contract.component';
import {OrdersComponent} from './orders/orders.component';
import {RestorePasswordComponent} from './restore-password/restore-password.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {DeliveryAddressComponent} from './delivery-address/delivery-address.component';
import {SuccessPayComponent} from './success-pay/success-pay.component';
import {ErrorPayComponent} from './error-pay/error-pay.component';
import {SuccessInstallmentComponent} from './success-installment/success-installment.component';
import {DelayComponent} from './delay/delay.component';
import {SettingsComponent} from './settings/settings.component';
import {ConfidentialityComponent} from './confidentiality/confidentiality.component';

const routes: Routes = [
  {
    path: '',
    component: MainProductComponent
  },
  {
    path: 'signup',
    component: RegistrationComponent
  },
  {
    path: 'confirmation',
    component: SmsConfirmationComponent
  },
  {
    path: 'decision',
    component: DecisionComponent
  },
  {
    path: 'loader',
    component: LoaderComponent
  },
  {
    path: 'restore-password',
    component: RestorePasswordComponent
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'sign-in/:id',
    component: SignInComponent
  },
  {
    path: 'contract',
    component: ContractComponent
  },
  {
    path: 'orders',
    component: OrdersComponent
  },
  {
    path: 'delivery-address',
    component: DeliveryAddressComponent
  },
  {
    path: 'success-pay',
    component: SuccessPayComponent
  },
  {
    path: 'error-pay',
    component: ErrorPayComponent
  },
  {
    path: 'success-installment',
    component: SuccessInstallmentComponent
  },
  {
    path: 'delay',
    component: DelayComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: 'confidentiality',
    component: ConfidentialityComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
