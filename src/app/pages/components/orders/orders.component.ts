import {Component, Inject, Input, OnInit} from '@angular/core';
import {ProductDTO} from '../../model/productDTO';
import {ProductService} from '../../service/product.service';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  product: ProductDTO;
  products: ProductDTO[];
  activeProducts: ProductDTO[] = [];
  closedProducts: ProductDTO[] = [];
  isLoading = false;

  payed = 5;
  flag = false;
  constructor(private productService: ProductService,
              @Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
    this.productService.getAll().subscribe(res => {
      this.products = res;
      this.products[0].isActive = true;

      this.products.forEach(item => {
        if (item.status === 'Закрыт') {
          this.closedProducts.push(item);
        } else {
          this.activeProducts.push(item);
        }
      });
    });
  }

  clickEvent(product: ProductDTO): void {
    this.products.forEach(item => {
      item.isActive = false;
    });
    product.isActive = true;
    this.product = product;
  }

  openLoader(): void {
    this.sharedService.startLoader();
    this.isLoading = true;
  }

  delay(ms: number): Promise<any> {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}
