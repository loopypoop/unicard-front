import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './error-pay.component.html',
  styleUrls: ['./error-pay.component.css']
})
export class ErrorPayComponent implements OnInit {

  payed = 1;
  next = false;

  constructor() { }

  ngOnInit(): void {
  }

  toNext(): void {
    this.next = !this.next;
  }

}
