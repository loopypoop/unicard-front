import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import {MainProductComponent} from './main-product/main-product.component';
import {FormsModule} from '@angular/forms';
import { RegistrationComponent } from './registration/registration.component';
import { SmsConfirmationComponent } from './sms-confirmation/sms-confirmation.component';
import { DecisionComponent } from './decision/decision.component';
import { LoaderComponent } from './loader/loader.component';
import {RestorePasswordComponent} from './restore-password/restore-password.component';
import {SignInComponent} from './sign-in/sign-in.component';
import { ContractComponent } from './contract/contract.component';
import { OrdersComponent } from './orders/orders.component';
import {DeliveryAddressComponent} from './delivery-address/delivery-address.component';
import {SuccessPayComponent} from './success-pay/success-pay.component';
import {ErrorPayComponent} from './error-pay/error-pay.component';
import {SuccessInstallmentComponent} from './success-installment/success-installment.component';
import {DelayComponent} from './delay/delay.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import {NumberFormatPipe} from '../pipes/number-format.pipe';
import {SettingsComponent} from './settings/settings.component';
import {ConfidentialityComponent} from './confidentiality/confidentiality.component';


@NgModule({
  declarations: [MainProductComponent, RegistrationComponent, SmsConfirmationComponent,
    DecisionComponent, LoaderComponent, RestorePasswordComponent, ContractComponent,
    SignInComponent, OrdersComponent, DeliveryAddressComponent, SuccessPayComponent, ErrorPayComponent,
    SuccessInstallmentComponent, DelayComponent, ProductDetailComponent, NumberFormatPipe,
    SettingsComponent, ConfidentialityComponent],
    imports: [
        CommonModule,
        PagesRoutingModule,
        FormsModule
    ],
  exports: [
    NumberFormatPipe
  ]
})
export class PagesModule { }
