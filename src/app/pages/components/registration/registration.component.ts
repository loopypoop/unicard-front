import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  isNameFocused = false;
  isEmailFocused = false;
  isPhoneFocused = false;
  isPwdFocused = false;
  isIinFocused = false;
  isIdNumberFocused = false;
  isIssuedFocused = false;

  fullName: string;
  email: string;
  phone: string;
  password: string;
  mainInfoEntered = false;
  iin: number;
  idNumber: number;
  issued: string;
  expireDate: Date;
  birthDate: Date;

  constructor() { }

  ngOnInit(): void {
  }

  toNextForm(): void {
    this.mainInfoEntered = true;
  }
}
