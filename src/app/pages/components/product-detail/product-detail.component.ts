import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {ProductService} from '../../service/product.service';
import {Product} from '../../model/product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit, OnChanges {

  @Input() productId: number;

  product: Product;
  priceString: string;
  payed = 3;

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    console.log('check 0: ', this.product);
    this.productService.getAll().subscribe(res => {
      this.product = res[0];
      console.log('check 1: ', this.product);
    });
  }

  ngOnChanges(): void {
    this.productService.getById(this.productId).subscribe(res => {
      this.product = res;
    });
  }

}
