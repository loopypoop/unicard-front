import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './success-pay.component.html',
  styleUrls: ['./success-pay.component.css']
})
export class SuccessPayComponent implements OnInit {

  payed = 1;
  next = false;

  constructor() { }

  ngOnInit(): void {
  }

  toNext(): void {
    this.next = !this.next;
  }

}
