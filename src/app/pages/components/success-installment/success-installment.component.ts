import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './success-installment.component.html',
  styleUrls: ['./success-installment.component.css']
})
export class SuccessInstallmentComponent implements OnInit {

  payed = 1;
  next = false;

  constructor() { }

  ngOnInit(): void {
  }

  toNext(): void {
    this.next = !this.next;
  }

}
