import {Component, Inject, OnInit} from '@angular/core';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-sms-confirmation',
  templateUrl: './sms-confirmation.component.html',
  styleUrls: ['./sms-confirmation.component.css']
})
export class SmsConfirmationComponent implements OnInit {
  showErrorAlert: boolean;
  activeInput: number;
  inputOne: string;
  inputTwo: number;
  inputThree: number;
  inputFour: number;
  inputFive: number;
  inputSix: number;
  isLoading = false;
  constructor(@Inject(SharedService) private sharedService) { }

  ngOnInit(): void {
    this.activeInput = 1;
    this.showErrorAlert = false;
  }

  ErrorAlert(): void {
    this.showErrorAlert = true;
  }

  hideErrorAlert(): void{
    this.showErrorAlert = false;
  }

  movetoTwo(): void{
    this.activeInput = 2;
  }

  openLoader(): void {
    this.sharedService.startLoader();
    this.isLoading = true;
  }
}
