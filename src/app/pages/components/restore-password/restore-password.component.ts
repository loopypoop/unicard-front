import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.css']
})
export class RestorePasswordComponent implements OnInit {

  isPwdFocused = false;
  isRePwdFocused = false;

  password: string;
  repassword: string;
  isRePasswordVisible: boolean;
  issued: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  changeRePasswordVisibility(): void {
    this.isRePasswordVisible = !this.isRePasswordVisible;
  }

  toNextForm(): void {
    this.router.navigate(['/sign-in/', 1]).then(() => {
      window.location.reload();
    });
  }
}
