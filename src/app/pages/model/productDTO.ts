export class ProductDTO {
  id: number;
  name: string;
  amountPayed: number;
  price: number;
  payDate: Date;
  provider: string;
  status: string;
  isActive = false;

  constructor() { }
}
