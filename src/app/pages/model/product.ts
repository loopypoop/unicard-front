export class Product {
  id: number;
  name: string;
  amountPayed: number;
  price: number;
  payDate: Date;
  provider: string;
  firstPayment: Date;
  installmentTerm: string;
  monthlyPayment: number;
  status: string;

  constructor() {}
}
