import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberFormat'
})
export class NumberFormatPipe implements PipeTransform {

  transform(value: number, args?: any): string {
    console.log('check 2: ', value);
    return value.toLocaleString();
  }

}
