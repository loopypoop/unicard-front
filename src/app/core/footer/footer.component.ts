import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  route: string;

  constructor(private router: Router, private location: Location) { }

  ngOnInit(): void {
    this.router.events.subscribe(val => {
      if (this.location.path() !== '') {
        this.route = this.location.path();
      } else {
        this.route = 'home';
      }
      console.log(this.route);
    });
  }

}
