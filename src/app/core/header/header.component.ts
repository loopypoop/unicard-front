import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {SharedService} from '../../pages/service/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuthenticated = true;
  route: string;
  loader: boolean;

  constructor(private router: Router,
              private location: Location,
              @Inject(SharedService) private sharedService) {
  }

  ngOnInit(): void {
    this.router.events.subscribe(val => {
      if (this.location.path() !== '') {
        this.route = this.location.path();
      } else {
        this.route = 'home';
      }
      console.log(this.route);
    });

    this.sharedService.isLoading.subscribe(val => {
      this.loader = val;
    });
    console.log('LOADER: ', this.loader);
  }

}
